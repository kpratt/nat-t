package upnp

import (
	syncth "github.com/syncthing/syncthing/lib/upnp"
	"time"
	"net"
	"github.com/pkg/errors"
)

// It's amazing how many uPnP libs there are
// that don't !@#$ work. Let's keep them at arms length
var Impl UPnP = syncthImpl{}

type UPnP interface {
	OpenExternalPort(internalPort, desiredExternalPort uint16) (externalIP string, aquiredExternalPort uint16, closer func(), err error)
}

type syncthImpl struct {}

func (impl syncthImpl) OpenExternalPort(internalPort, desiredExternalPort uint16) (externalIP string, aquiredExternalPort uint16, closer func(), err error) {
	devices := syncth.Discover(3 * time.Second, 10 * time.Second)
	closer = func(){}

	var port int
	var netIP net.IP
	for _, device := range devices {
		netIP, err = device.GetExternalIPAddress()
		if err != nil {
			return
		}
		externalIP = netIP.String()

		port, err = device.AddPortMapping("TCP", int(internalPort), int(desiredExternalPort), "upnp-test", 60 * time.Second)
		if err != nil {
			return
		}

		aquiredExternalPort = uint16(port)
		return
	}

	err = errors.New("No devices found")
	return
}
