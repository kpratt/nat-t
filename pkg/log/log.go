package log

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"runtime"
	"fmt"
)

var log *zap.Logger

func init() {
	config := zap.Config{
		Level:       zap.NewAtomicLevelAt(zap.DebugLevel),
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "json",
		EncoderConfig:    zap.NewProductionEncoderConfig(),
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stdout"},
	}
	log, _ = config.Build(
		zap.AddCallerSkip(1),
	)
}

func Info(msg string, fields ...zapcore.Field) {
	log.Info(msg, fields...)
}

func Debug(msg string, fields ...zapcore.Field) {
	log.Debug(msg, fields...)
}

func ThreadDump(logmsg string) {
	buf := make([]byte, 1<<20)
	stacklen := runtime.Stack(buf, false)
	Info("Stack Dump...\n"+logmsg, zap.Stringer("stack", Hex(buf[:stacklen])))
}

func Sync() {
	log.Sync()
}

type Hex []byte

func (bs Hex) String() string {
	return fmt.Sprintf("%#x", []byte(bs))
}
