
.PHONY: all test clean debug
test:
	@go test -timeout 30s ./...

ci:
	@go get -v ./...
	@go get github.com/golang/mock/gomock
	@go test -timeout 30s ./...

install:
	@go install ./cmd/...





