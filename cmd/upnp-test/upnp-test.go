package main

import (
	"net/http"
	"fmt"
	"time"
	"bitbucket.org/kpratt/nat-t/pkg/log"

	"go.uber.org/zap"
	"bitbucket.org/kpratt/nat-t/pkg/upnp"
)

func testConnectivity(ip string, port uint16) bool {
	c := http.DefaultClient
	_, err := c.Get(fmt.Sprintf("http://%s:%d/", ip, port))
	if err != nil {
		log.Info("HTTP Client Error", zap.Error(err))
		return false
	}
	return true
}

func main() {
	defer log.Sync()

	var internalPort uint16 = 65000

	go http.ListenAndServe(fmt.Sprintf(":%d", internalPort), http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	}))

	time.Sleep(100 * time.Millisecond)
	if !testConnectivity("127.0.0.1", internalPort) {
		log.Info("Server not available internally")
		return
	}

	ip, port, closer, err := upnp.Impl.OpenExternalPort(internalPort, internalPort)
	if err != nil {
		log.Info("UPnP failed", zap.Error(err))
		return
	}

	func() {
		defer closer()
		log.Info(
			"UPnP established",
			zap.String("ip", ip),
			zap.Uint16("port", port),
		)

		if !testConnectivity(ip, port) {
			log.Info("Server not available internally")
		}
	}()

	if !testConnectivity(ip, port) {
		log.Info("Server still available after shutdown")
	}

}
